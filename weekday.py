import calendar

days =["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"] 

def get_week_day(date):
	year, month, day = (int(i) for i in date.split("-"))
	day = calendar.weekday(year, month, day)
	return day

def findMissing(output, counter, currIndex, lastIndex):

	if output[currIndex-1] == None:
		findMissing(output, counter+1, currIndex-1,lastIndex)

	output[currIndex] = int((counter*output[currIndex-1] + output[lastIndex])/(counter+1))
	return None


def solution(d):
	output = [None]*(len(days))
	for key in d.keys():
		day = get_week_day(key)
		if output[day] == None:
			output[day] = 0
		output[day] = output[day] + d[key]

	#Since both Sunday and Monday are present beforehand
	i = len(output) - 2

	while i >= 1:
		prev = i - 1
		nxt  = i + 1
		if output[i] == None:
			findMissing(output,1,i,nxt)
		i = prev

	finalOutput = {days[i] : output[i] for i in range(0,len(days))}

	return finalOutput

if __name__ == '__main__':
	d = {"2020-01-01": 4, "2020-01-02": 4, "2020-01-03": 6, "2020-01-04": 8, "2020-01-05": 2, "2020-01-06": -6, "2020-01-07": 2, "2020-01-08": -2}
	print(solution(d))