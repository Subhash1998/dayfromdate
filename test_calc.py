import unittest
import weekday
import datetime

class TestCalc(unittest.TestCase):

	d1 = {"2020-01-01": 4, "2020-01-02": 4, "2020-01-03": 6, "2020-01-04": 8, "2020-01-05": 2, "2020-01-06": -6, "2020-01-07": 2, "2020-01-08": -2}
	d2 = {"2020-01-05": 14, "2020-01-06": 2}

	def test_date(self):
		self.assertEqual(weekday.get_week_day("2020-01-06"), 0)
		self.assertEqual(weekday.get_week_day("2020-01-07"), 1)
		self.assertEqual(weekday.get_week_day("2020-01-01"), 2)
		self.assertEqual(weekday.get_week_day("2020-01-02"), 3)
		self.assertEqual(weekday.get_week_day("2020-01-03"), 4)
		self.assertEqual(weekday.get_week_day("2020-01-04"), 5)
		self.assertEqual(weekday.get_week_day("2020-01-05"), 6)

	def test_solution(self):
		self.assertEqual(weekday.solution(self.d1),{'Mon': -6, 'Tue': 2, 'Wed': 2, 'Thu': 4, 'Fri': 6, 'Sat': 8, 'Sun': 2})
		self.assertEqual(weekday.solution(self.d2),{'Mon': 2, 'Tue': 4, 'Wed': 6, 'Thu': 8, 'Fri': 10, 'Sat': 12, 'Sun': 14})

if __name__ == '__main__':
	unittest.main()